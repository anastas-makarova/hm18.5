﻿
#include <iostream>
#include <string>


class Player
{
public:
    std::string Name;
    int Scope;

    Player (std::string _name = "Anonimus", int _scope = 0) : Name(_name), Scope(_scope)
    {
    }
};

void BubbleSort(Player* ArrayPlayers, int CountPlayers)
{
    bool b = true;
    while (b)
    {
        b = false;
        for (int i = 0; i < CountPlayers - 1; i++)
        {
            if (ArrayPlayers[i].Scope < ArrayPlayers[i + 1].Scope)
            {
                std::swap(ArrayPlayers[i], ArrayPlayers[i + 1]);
                b = true;
            }
        }
    }
}

void SetPlayerInfo(Player* ArrayPlayers, int CountPlayers)
{
    for (int i = 0; i < CountPlayers; i++)
    {
        std::cout << "Player " << i+1 << ": "; 
        std::cout << "\n Name: ";
      
        std::cin >> ArrayPlayers[i].Name;
      
        std::cout << "Score: ";

        std::cin >> ArrayPlayers[i].Scope;
    }
}

int main()
{
    int CountPlayers;
   

    std::cout << "Enter the number of players: ";
    std::cin >> CountPlayers;
    auto ArrayPlayers = new Player[CountPlayers];

    SetPlayerInfo(ArrayPlayers, CountPlayers);

    BubbleSort(ArrayPlayers, CountPlayers);

    std::cout << "\n\n Results :\n";
    for (int i = 0; i < CountPlayers; i++)
    {
        std::cout << ArrayPlayers[i].Name << " Have(has) " << ArrayPlayers[i].Scope << " points\n";
    }

    delete[] ArrayPlayers;
    return 0;
}
